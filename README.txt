To run the application

- composer install on root folder
- change .env values or create a .env.local and add your database data (You have the dump on root folder)
- You can run $ php7.3 bin/console server:run if you want to user the built in php server.
