<?php

namespace App\Model\Service;

use App\Model\Entity\User;
use App\Model\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;


/**
 * Class SignUp
 * @package App\Service
 */
class SignUp
{
    private $session;
    private $em;
    private $logger;
    private $userRepository;

    // Static variables that identifies the fields to fulfil in each step

    public static $STEP_ONE_VALIDATION = ['name', 'lastname', 'phone'];
    public static $STEP_TWO_VALIDATION = ['street', 'number', 'city', 'zipcode'];
    public static $STEP_THREE_VALIDATION = ['account_owner', 'iban', 'payment_data_id'];
    public static $STEP_FOUR_VALIDATION = ['payment_data_id'];

    CONST STEP_ONE = 'one';
    CONST STEP_TWO = 'two';
    CONST STEP_THREE = 'three';
    CONST STEP_FOUR = 'four';

    CONST API_URL = 'https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data';


    public function __construct(SessionInterface $session, EntityManagerInterface $em, UserRepository $userRepository, LoggerInterface $logger)
    {
        $this->session = $session;
        $this->em = $em;
        $this->userRepository = $userRepository;
        $this->logger = $logger;
        $this->initializeSession();
    }

    /**
     * @return SignUp
     * Define in which step we are base on user data saved on database
     */

    public function stepResolver(): SignUp
    {
        // initial state whenever we try
        $this->setCurrentStep(SELF::STEP_ONE);

        $sessionId = $this->initializeSession();
        $User = $this->userRepository->findOneBy(['session_id' => $sessionId]);
        if ($User instanceof User) {

            $userArray = $User->toArray();
            foreach (self::$STEP_ONE_VALIDATION as $key) {
                if (empty($userArray[$key])) {
                    return $this;
                }
            }

            // we reach at least step 2 so far
            $this->setCurrentStep(SELF::STEP_TWO);

            foreach (self::$STEP_TWO_VALIDATION as $key) {
                if (empty($userArray[$key])) {
                    return $this;
                }
            }

            // here we are, last step
            $this->setCurrentStep(SELF::STEP_THREE);

            // steps are done
            foreach (self::$STEP_THREE_VALIDATION as $key) {
                if (empty($userArray[$key])) {
                    return $this;
                }
            }

            $this->setCurrentStep(SELF::STEP_FOUR);

        }
        return $this;
    }

    /**
     * @return string
     */
    public function getTemplatePathToActualStep() : string
    {
        return \sprintf('SignUp/step-%s.html.twig', $this->stepResolver()->getCurrentStep());
    }

    /**
     * @return string
     */
    public function initializeSession(): string
    {
        if ($this->session->get('current_step') === null) {
            $this->session->set('current_step', 'one');
        }

        return $this->session->getId();
    }

    /**
     * @return string
     */
    public function getCurrentStep() : string
    {
        return $this->session->get('current_step');
    }

    /**
     * @param string $step
     * @return SignUp
     */
    private function setCurrentStep(string $step) : SignUp
    {
        $this->session->set('current_step', $step);
        return $this;
    }

    /**
     * @param Request $request
     * @return bool
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function saveUserData(Request $request): bool
    {
        $sessionId = $this->initializeSession();
        $userData = $this->userRepository->findOneBy(['session_id' => $sessionId]);
        if (!$userData instanceof User) {
            $userData = new User();
        }

        $userData->setSessionId($sessionId);
        switch ($this->getCurrentStep()) {
            case 'one' :
                $userData->setName($request->get('name'));
                $userData->setLastname($request->get('lastname'));
                $userData->setPhone($request->get('phone'));
                break;
            case 'two' :
                $userData->setStreet($request->get('street'));
                $userData->setNumber($request->get('number'));
                $userData->setZipcode($request->get('zipcode'));
                $userData->setCity($request->get('city'));
                break;
            case 'three' :
                $userData->setAccountOwner($request->get('accountowner'));
                $userData->setIban($request->get('iban'));
                $paymentDataId = $this->registerOnAPi($userData);
                if (empty($paymentDataId)) {
                    return false;
                }
                $userData->setPaymentDataId($paymentDataId);
                break;
        }

        $this->em->persist($userData);
        $this->em->flush();

        return true;

    }

    /**
     * @return $this
     */
    public function resetUserData()
    {
        $sessionId = $this->initializeSession();
        $userData = $this->userRepository->findOneBy(['session_id' => $sessionId]);
        if ($userData instanceof User) {
            $this->em->remove($userData);
            $this->em->flush();
        }
        return $this;
    }

    /**
     * @param User $User
     * @return string|null
     */
    protected function registerOnAPi(User $User): ?string
    {
        try {
            $client = HttpClient::create();
            $response = $client->request('POST', SELF::API_URL, [
                'body' => json_encode([
                    'customerId' => $User->getId(),
                    'iban' => $User->getIban(),
                    'owner' => $User->getAccountOwner()
                ])
            ]);

            switch ($response->getStatusCode()) {
                case Response::HTTP_OK:

                    $content = json_decode($response->getContent(), true);
                    if (array_key_exists('paymentDataId', $content)) {
                        return $content['paymentDataId'];

                    }
                    throw new \Exception('The response doesn`t looks good');
                default:
                    // as long we're waiting only for 200 Response, anything else will be logged
                    throw new \Exception($response->getInfo());

            }
        } catch (\Throwable $e) {
            // we're treating exceptions generally because any expected behavior was defined on the specification
            $this->logger->error(\sprintf('Can`t save bank data over the api: %s', $e->getMessage()), $e->getTrace());
        }
        return null;
    }

    /**
     * @return User|null
     */
    public function getUserData(): ?User
    {
        $sessionId = $this->initializeSession();
        return $this->userRepository->findOneBy(['session_id' => $sessionId]);
    }
}

