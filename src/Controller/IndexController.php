<?php

namespace App\Controller;

use App\Model\Service\SignUp;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{

    /**
     * @param SignUp $signUpService
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @Route("/signup", name="signup")
     */
    public function index(SignUp $signUpService, Request $request)
    {
        try {
            if ($request->isMethod('POST')) {
                if ($signUpService->saveUserData($request)) {
                    $signUpService->stepResolver();
                } else {
                    throw new \Exception("Could not save your data.");
                }
            }
            $userData = $signUpService->getUserData();
            $templateName = $signUpService->getTemplatePathToActualStep();
            return $this->render($templateName, ['User' => $userData]);
        } catch (\Throwable $e) {
            $this->addFlash('error', \sprintf('The application crashed (%s)', $e->getMessage()));
            return $this->redirectToRoute('signup');
        }
    }

    /**
     * @param SignUp $signUpService
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @Route("/reset", name="reset")
     */
    public function reset(SignUp $signUpService)
    {
        $signUpService->resetUserData();
        return $this->redirectToRoute('home');
    }


    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/", name="home")
     */
    public function home()
    {
        return $this->render('home.html.twig');
    }
}
